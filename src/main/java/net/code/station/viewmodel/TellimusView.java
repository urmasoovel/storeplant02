package net.code.station.viewmodel;

import java.sql.Date;

import net.code.station.model.Contact;
import net.code.station.model.Pakkumine;
import net.code.station.model.Periood;
import net.code.station.model.Staatus;

public class TellimusView {
	private Integer id;
	private Pakkumine pakkumine;
	private Contact klient;
	private Date alates;
	private Date kuni;
	private Periood periood;
	private Staatus staatus;
	private Integer rollid;//muutja roll	
	public TellimusView() {
		
	}
	public TellimusView(Integer id, Pakkumine pakkumine, Contact klient,
			 Date alates, Date kuni, Periood periood, Staatus staatus,
			 Integer rollid) {
		this(pakkumine, klient, alates, kuni, periood, staatus, rollid);
		this.id = id;
	}
	public TellimusView(Pakkumine pakkumine, Contact klient,
			Date alates, Date kuni, Periood periood, Staatus staatus, 
			Integer rollid) {
		this.pakkumine = pakkumine;
		this.klient = klient;
		this.alates = alates;
		this.kuni = kuni;
		this.periood = periood;
		this.staatus = staatus;
		this.rollid = rollid;		
	}
	public Integer getId() {return id;}
	public void setId(Integer id) {this.id = id;}
	public Pakkumine getPakkumine() {return pakkumine;}
	public void setPakkumine(Pakkumine pakkumine) {this.pakkumine = pakkumine;}
	public Contact getKlient() {return klient;}
	public void setKlient(Contact klient) {this.klient = klient;}	
	public Periood getPeriood() {return periood;}
	public void setPeriood(Periood periood) {this.periood = periood;}
	public Staatus getStaatus() {return staatus;}
	public void setStaatus(Staatus staatus) {this.staatus = staatus;}
	public Integer getRollid() {return rollid;}
	public void setRollid(Integer rollid) {this.rollid = rollid;}
	@Override
	public String toString() {
		return "TellimusView [id=" + id + ", pakkumine=" + pakkumine + ", klient=" + klient +  "]";
	}
}
