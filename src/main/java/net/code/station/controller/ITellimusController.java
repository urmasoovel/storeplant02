package net.code.station.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.code.station.model.Periood;
import net.code.station.model.Tellimus;
import net.code.station.viewmodel.TellimusView;

public interface ITellimusController {
	public String newTellimus(Model model
			, Authentication authentication);
	public String valiPerioodTellimusele(
			@Valid @ModelAttribute("tellimus") Tellimus tellimus,
			Errors errors,
			Model model, 			
			HttpServletRequest request,
			Authentication authentication,
			HttpServletResponse response
			) throws IOException;
	private List<Periood> filtreeriPerioodid(String userName) {
		return null;
	}
	public String kuvaTellimusForm(Model model,
			Authentication authentication,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException;
	public String saveTellimus(
			@Valid @ModelAttribute("tellimus") Tellimus tellimus,
			Errors errors,
			Model model,			
			HttpServletRequest request, 
			Authentication authentication,
			HttpServletResponse response) throws IOException;
	public ModelAndView saveTellimusPersonal(ModelAndView model,
			@ModelAttribute Tellimus tellimus);
	public ModelAndView editTellimus(HttpServletRequest request,
			Authentication authentication);
	
	public ModelAndView minuTellimused(ModelAndView model, 
			HttpServletRequest request, Authentication authentication);
	private List<TellimusView> koostaListTellimusView(List<Tellimus> listTellimus,
			Integer klientid, Integer rollid) {
		return null;
	}
	public void deleteTellimus(ModelAndView model,
			HttpServletRequest request,	HttpServletResponse response, 
			Authentication authentication) throws IOException;
	public ModelAndView tellimusteAjalugu(ModelAndView model, 
			HttpServletRequest request, Authentication authentication);
	
	
	
}
