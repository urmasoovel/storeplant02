package net.code.station.dao;

import net.code.station.model.Bilansihaldur;

public interface BilansihaldurDAO {

	Bilansihaldur get(Integer bilansihaldurid);

}
