package net.code.station.dao;

import net.code.station.model.Arvesti;

public interface ArvestiDAO {
	public Arvesti get(Integer id);
}
